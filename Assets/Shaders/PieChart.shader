﻿Shader "Custom/PieChart" {
	Properties {
		_Value ("Value", Range(0, 360)) = 30
		_MainColor ("Main Color", Color) = (1, 1, 1, 1)
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert

		float _Value;
		float4 _MainColor;
		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			float2 vec = IN.uv_MainTex - float2(0.5, 0.5);
			if (length(vec) > 0.5) {
				clip(-1);
				return;
			}
			vec = normalize(vec);
			float rad = radians(_Value);
			float c = cos(rad);
			float d = dot(float2(0, 1), vec);
			if (_Value <= 180 && (vec.x >= 0 && d > c)) {
				half4 c = tex2D (_MainTex, IN.uv_MainTex);
				o.Albedo = c.rgb * _MainColor.rgb;
				o.Alpha = c.a * _MainColor.a;
				return;
			}
			if (_Value > 180 && (vec.x >= 0 || d < c)) {
				half4 c = tex2D (_MainTex, IN.uv_MainTex);
				o.Albedo = c.rgb * _MainColor.rgb;
				o.Alpha = c.a * _MainColor.a;
				return;
			}
			clip(-1);
			return;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
